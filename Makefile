PACKAGE=mqrd
VERSION=0.1
CFLAGS=-ggdb
CPPFLAGS=-DVERSION=\"$(VERSION)\"
LIBS=
PREFIX=/usr
SBINDIR=$(PREFIX)/sbin
MANDIR=$(PREFIX)/man

SRCS=mqrd.c
OBJS=$(SRCS:.c=.o)

DISTFILES=Makefile $(SRCS) mqrd.8 instlist

mqrd: $(OBJS)
	cc -omqrd $(CFLAGS) $(OBJS) $(LIBS)

.c.o:
	cc -c $(CPPFLAGS) $(CFLAGS) $<

install-bin: mqrd
	install -D mqrd $(DESTDIR)$(SBINDIR)/mqrd

install-man: mqrd.8
	install -D mqrd.8 $(DESTDIR)$(MANDIR)/man8/mqrd.8

install: install-bin install-man

distdir = $(PACKAGE)-$(VERSION)

distdir:
	rm -rf $(distdir)
	mkdir $(distdir)
	cp $(DISTFILES) $(distdir)

dist: distdir
	tar cfz $(distdir).tar.gz $(distdir)
	rm -rf $(distdir)

distcheck: distdir
	mkdir $(distdir)/_inst; \
        cd $(distdir) || exit 2;\
        make || exit 2; \
        make DESTDIR=`pwd`/_inst install || exit 2
	(cd $(distdir)/_inst; find . -type f)|sort|cut -c2- | \
           diff - instlist 
	make dist

clean:
	rm -f mqrd $(OBJS)

