/* This file is part of mqrd.
   Copyright (C) 2012 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sysexits.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <glob.h>
#include <syslog.h>
#include <signal.h>
#include <errno.h>
#include <grp.h>

char *progname;
int dry_run;
int single_process;
int terminate;
long timeout_option = 5;
int debug_level;

#define dbg(n) (debug_level>=(n))

#ifndef SYSLOG_FACILITY
# define SYSLOG_FACILITY LOG_MAIL
#endif

#ifndef VERSION
# define VERSION "0.1"
#endif

#ifndef MQUEUE_DIR
# define MQUEUE_DIR "/var/spool/mqueue"
#endif

#ifndef SENDMAIL
# define SENDMAIL "/usr/sbin/sendmail"
#endif

void
usage(FILE *stream)
{
	fprintf(stream, "usage: %s [OPTIONS] SOCKET\n", progname);
	fprintf(stream, "\nOPTIONS are:\n\n");
	fprintf(stream, "  -f        remain in the foreground\n");
	fprintf(stream, "  -g NAME   set owner group of the SOCKET to NAME\n");
	fprintf(stream, "  -h        show this help summary\n");
	fprintf(stream, "  -n        do nothing, print what would have been done\n");
	fprintf(stream, "  -m NUM    set maximum number of children\n");
	fprintf(stream, "  -M MODE   set socket mode (octal)\n");
	fprintf(stream, "  -p FILE   write PID to FILE\n");
	fprintf(stream, "  -r        remove socket if it exists\n");
	fprintf(stream, "  -s        single process mode\n");
	fprintf(stream, "  -v        print program version and exit\n");
	fprintf(stream, "\n");
}

void
version()
{
	printf("mqrd " VERSION "\n");
	printf("Copyright (C) 2012 Sergey Poznyakoff\n");
	printf("License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\n"
	       "This is free software: you are free to change and redistribute it.\n"
	       "There is NO WARRANTY, to the extent permitted by law.\n");
}

unsigned long nchildren = 0, maxchildren = 64;

void
sigterm(int sig)
{
	terminate = 1;
}

void
sigchild(int sig)
{
	pid_t pid;
	int status;
	
	while ((pid = waitpid((pid_t) -1, &status, WNOHANG)) > 0)
		nchildren--;
}

typedef void (*sig_handler_t)(int);

static sig_handler_t
set_signal(int sig, sig_handler_t handler)
{
	struct sigaction act, oldact;
	act.sa_handler = handler;
	sigemptyset(&act.sa_mask);
	act.sa_flags = 0;
	sigaction(sig, &act, &oldact);
	return oldact.sa_handler;
}

static void
close_fds(fd_set *fdset)
{
	int i;

	for (i = sysconf(_SC_OPEN_MAX); i >= 0; i--) {
		if (fdset && FD_ISSET(i, fdset))
			continue;
		close(i);
	}
}

static int
trimcrlf(char *buf)
{
	size_t len = strlen(buf);
	if (len && buf[len-1] == '\n') {
		--len;
		if (buf[len-1] == '\r')
			--len;
		buf[len] = 0;
		return 0;
	}
	return -1;
}		

int
deliver_message(const char *id)
{
	char *argv[5];
	int i;
	pid_t pid;
	int p[2];
	fd_set fdset;
	char *buf = malloc(strlen(id) + 4);
	char msgbuf[512];
	FILE *fp;
	int ret = 0;
	time_t start;
	int intr, status, rc;
	fd_set rd;
	struct timeval tv;
	long ttl;
	
	if (dbg(1))
		syslog(LOG_DEBUG, "delivering %s", id);

	if (!buf) {
		syslog(LOG_ERR, "not enough memory");
		return -1;
	}
	strcpy(buf, "-qI");
	strcat(buf, id);
	
	i = 0;
	if (dry_run)
		argv[i++] = "/bin/echo";
	argv[i++] = SENDMAIL;
	argv[i++] = "-qQ";
	argv[i++] = buf;
	argv[i++] = NULL;

	pipe(p);
	pid = fork();

	if (pid == -1) {
		syslog(LOG_ERR, "fork: %m");
		free(buf);
		close(p[0]);
		close(p[1]);
		return -1;
	}
	
	if (pid == 0) {
		/* child */
		dup2(p[1], 1);
		dup2(p[1], 2);
		
		FD_ZERO(&fdset);
		FD_SET(0, &fdset);
		FD_SET(1, &fdset);
		FD_SET(2, &fdset);
		close_fds(&fdset);
		execv(argv[0], argv);
		_exit(127);
	}

	/* master */
	close(p[1]);

	start = time(NULL);
	intr = 0;
	rc = 0;
	status = 0;
	for (i = 0; ;) {
		FD_ZERO(&rd);
		FD_SET(p[0], &rd);

		if (intr) {
			rc = waitpid(pid, &status, WNOHANG);
			if (rc == pid)
				break;
			if (rc == (pid_t)-1) {
				syslog(LOG_ERR, "waitpid: %m");
				ret = -1;
				break;
			}
			intr = 0;
		}
		ttl = timeout_option - (time(NULL) - start);
		if (ttl <= 0) {
			syslog(LOG_ERR, "timed out reading from %s", argv[0]);
			ret = -1;
			break;
		}
		tv.tv_sec = ttl;
		tv.tv_usec = 0;
		rc = select(p[0] + 1, &rd, NULL, NULL, &tv);
		if (rc < 0) {
			if (errno == EINTR || errno == EAGAIN) {
				intr = 1;
				continue;
			}
			syslog(LOG_ERR, "select: %m");
		}
		if (i == sizeof(msgbuf) - 1) {
			msgbuf[i] = 0;
			syslog(LOG_INFO, "%s: %s\\", argv[0], msgbuf);
			i = 0;
		}
		if (FD_ISSET(p[0], &rd)) {
			char c;
			
			rc = read(p[0], &c, 1);
			if (rc == 1) {
				if (c == '\n') {
					msgbuf[i] = 0;
					syslog(LOG_INFO, "%s: %s",
					       argv[0], msgbuf);
					i = 0;
				} else
					msgbuf[i++] = c;
			} else if (rc == 0
				   || errno == EINTR || errno == EAGAIN) {
				intr = 1;
				continue;
			} else {
				syslog(LOG_ERR, "read: %m");
				ret = -1;
				break;
			}
		}
	}
	if (i) {
		msgbuf[i] = 0;
		syslog(LOG_INFO, "%s: %s", argv[0], msgbuf);
	}
	close(p[0]);

	if (rc != pid) {
		syslog(LOG_NOTICE, "killing %s (pid %lu)",
		       argv[0], (unsigned long) pid);
		kill(pid, SIGKILL);
		
		while ((rc = waitpid(pid, &status, 0)) == -1 &&
		       errno == EINTR);
		if (rc == (pid_t)-1) {
			syslog(LOG_ERR, "waitpid: %m");
		}
		ret = -1;
	} else if (WIFEXITED(status)) {
		status = WEXITSTATUS(status);
		if (status) {
			syslog(LOG_ERR, "%s exited with status %d",
			       argv[0], status);
			ret = -1;
		}
	} else if (WIFSIGNALED(status)) {
		status = WTERMSIG(status);
		syslog(LOG_ERR, "%s got signal %d", argv[0], status);
		return -1;
	} else if (status) {
	       syslog(LOG_ERR, "%s failed: unknown status 0x%x",
			 argv[0], status);
	       ret = -1;
	}

	return ret;
}

int
globerrfunc(const char *epath, int eerrno)
{
	syslog(LOG_ERR, "%s: %s", epath, strerror(eerrno));
	return 1;
}

/* The QID format is: YMDhmsNMPPPPP */
#define QIDLEN 14
/* The valid QID letters are: */
static const char qabc[] = "0123456789"
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	"abcdefghijklmnopqrstuvwxyz";
/* stem of a queue file (globbing pattern) */
#define QFSTEMGLOB "[dqtxh]f"

static int
valid_qid(const char *qid)
{
	int i;
	
	if (strlen(qid) != QIDLEN)
		return 0;
	for (i = 0; i < 8; i++)
		if (!strchr(qabc, qid[i]))
			return 0;
	for (; qid[i]; qid++)
		if (!isdigit(qid[i]))
			return 0;
	return 1;
}

int
drop_msg(const char *id)
{
	glob_t g;
	size_t i;
	char pat[sizeof(QFSTEMGLOB) + QIDLEN];
	int rc = 0;
	
	strcpy(pat, QFSTEMGLOB);
	strcat(pat, id);

	switch (glob(pat, 0, globerrfunc, &g)) {
	case 0:
		for (i = 0; i < g.gl_pathc; i++) {
			if (dbg(1))
				syslog(LOG_DEBUG, "removing %s",
				       g.gl_pathv[i]);
			if (!dry_run && unlink(g.gl_pathv[i])) {
				syslog(LOG_ERR, "can't remove %s: %m",
				       g.gl_pathv[i]);
				rc = -1;
			}
		}
		globfree(&g);
		break;
		
	case GLOB_NOSPACE:
		syslog(LOG_ERR, "glob: not enough memory");
		rc = -1;
		break;

	case GLOB_ABORTED:
		syslog(LOG_ERR, "glob aborted");
		rc = -1;
		break;
	}
	return rc;
}

void
process(FILE *fp)
{
	char inbuf[1024];
	size_t len;
	char *p;
	int rc;
	char *repl;
	
	if (!fgets(inbuf, sizeof(inbuf), fp)) {
		syslog(LOG_ERR, "fgets: %m");
		return;
	}
	if (dbg(2))
		syslog(LOG_DEBUG, "rcvd: %s", inbuf);
	if (trimcrlf(inbuf)) {
		syslog(LOG_INFO, "input line too long");
		if (!single_process) {
			/* drain input */
			int c;
			while ((c = fgetc(fp)) != EOF && c != '\n')
				;
		}
		fprintf(fp, "-ERR input too long\r\n");
		return;
	}
	if (inbuf[0] == 0) {
		syslog(LOG_INFO, "ignoring empty line");
		fprintf(fp, "-ERR syntax error\r\n");
		return;
	}

	for (p = inbuf; *p && *p != ' '; p++)
		;
	if (*p)
		*p++ = 0;

	if (!*p) {
		syslog(LOG_ERR, "invalid input line: %s", inbuf);
		fprintf(fp, "-ERR syntax error\r\n");
		return;
	}
	

	if (!valid_qid(p)) {
		syslog(LOG_ERR, "invalid QID: %s", p);
		fprintf(fp, "-ERR Invalid QID\r\n");
		return;
	} else if (strcmp(inbuf, "DROP") == 0)
		rc = drop_msg(p);
	else if (strcmp(inbuf, "DLVR") == 0) {
		rc = deliver_message(p);
	} else {
		syslog(LOG_ERR, "unrecogized command %s", inbuf);
		fprintf(fp, "-ERR unrecognized command\r\n");
		return;
	}

	if (rc)
		repl = "-ERR command failed";
	else
		repl = "+OK";
	if (dbg(2))
		syslog(LOG_DEBUG, "sent: %s", repl);
	fprintf(fp, "%s\r\n", repl);
}

int
check_pidfile(const char *pidfile)
{
	int c;
	pid_t n = 0;

	FILE *fp = fopen(pidfile, "r");
	if (!fp) {
		if (errno != ENOENT)
			fprintf(stderr, "%s: cannot open pid file `%s': %s",
				progname, pidfile, strerror(errno));
		return -1;
	}

	while ((c = fgetc(fp)) != EOF) {
		if (isdigit(c))
			n = n * 10 + c - '0';
		else if (c == '\n')
			break;
		else {
			fprintf(stderr,
				"%s: unexpected character %#03o "
				"in pidfile `%s'\n",
				c, pidfile);
			return -1;
		}
	}
	fclose(fp);

	if (n && kill(n, 0)) {
		if (errno != ESRCH)
			fprintf(stderr,
				"%s: cannot signal master process %lu: %s",
				progname, (unsigned long) n, strerror(errno));
		if (errno == EPERM)
			return n;               /* be on the safe side */
		return -1;
	}
	return n;
}

int
main(int argc, char **argv)
{
	int rc, sd;
	char *sname;
	struct sockaddr_un s_un;
	int foreground = 0;
	int remove = 0;
	char *pidfile = NULL;
	sig_handler_t oldchild;
	struct stat st;
	gid_t grid = 0;
	mode_t socket_mode = 0600;
	struct group *gr;
	char *tag;
	
	progname = argv[0];

	while ((rc = getopt(argc, argv, "dfg:hm:M:nrp:st:v")) != EOF) {
		switch (rc) {
		case 'd':
			debug_level++;
			break;
		case 'f':
			foreground = 1;
			break;
		case 'g':
			gr = getgrnam(optarg);
			if (!gr) {
				fprintf(stderr,
					"%s: no such group: %s\n", progname,
					optarg);
				exit(EX_NOUSER);
			}
			grid = gr->gr_gid;
			break;
		case 'h':
			usage(stdout);
			exit(0);
		case 'n':
			dry_run++;
			debug_level++;
			break;
		case 'm':
			maxchildren = strtoul(optarg, NULL, 10);
			break;
		case 'M':
			socket_mode = strtoul(optarg, NULL, 8) & 0777;
			break;
		case 'p':
			pidfile = optarg;
			break;
		case 'r':
			remove = 1;
			break;
		case 's':
			single_process++;
			break;
		case 't':
			timeout_option = strtol(optarg, NULL, 10);
			break;
		case 'v':
			version();
			exit(0);
		default:
			exit(EX_USAGE);
		}
	}


	if (optind != argc - 1) {
		usage(stderr);
		exit(EX_USAGE);
	}
	sname = argv[optind];
	if (strlen(sname) >= sizeof(s_un.sun_path)) {
		fprintf(stderr, "%s: socket name too long\n", progname);
		exit(EX_DATAERR);
	}

	if (pidfile) {
		pid_t p = check_pidfile(pidfile);
		if (p != (pid_t) -1) {
			fprintf(stderr,
				"%s: another instance already running (pid %lu)\n",
				progname, (unsigned long) p);
			exit(1);
		}
	}
		
	sd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (sd == -1) {
		fprintf(stderr, "%s: socket: %s\n", progname,
			strerror(errno));
		exit(errno == EACCES ? EX_NOPERM : EX_CANTCREAT);
	}

	if (stat(sname, &st) == 0) {
		if (!S_ISSOCK(st.st_mode)) {
			fprintf(stderr, "%s: %s is not a socket: %s\n",
				progname,
				sname, strerror(errno));
			exit(EX_CONFIG);
		}
		/* FIXME: access rights? */
		if (remove && unlink(sname)) {
			fprintf(stderr, "%s: can't unlink %s: %s\n",
				progname, sname, strerror(errno));
			exit(EX_UNAVAILABLE);
		}
	} else if (errno != ENOENT) {
		fprintf(stderr, "%s: can't stat %s: %s\n", progname,
			sname, strerror(errno));
		exit(errno == EACCES ? EX_NOPERM : EX_UNAVAILABLE);
	}

	umask(077);

	s_un.sun_family = AF_UNIX;
	strcpy(s_un.sun_path, sname);

	rc = bind(sd, (const struct sockaddr *)&s_un, sizeof(s_un));
	if (rc) {
		fprintf(stderr, "%s: can't bind to %s: %s\n", progname,
			sname, strerror(errno));
		exit(errno == EACCES ? EX_NOPERM : EX_CANTCREAT);
	}
		
	chown(sname, 0, grid);
	chmod(sname, socket_mode & ~0111);
	
	if (listen(sd, 8)) {
		fprintf(stderr, "%s: listen: %s\n", progname, strerror(errno));
		exit(errno == EACCES ? EX_NOPERM : EX_UNAVAILABLE);
	}
	
	if (chdir(MQUEUE_DIR)) {
		fprintf(stderr,
			"%s: can't change to %s: %s\n",
			progname, MQUEUE_DIR, strerror(errno));
		exit(errno == EACCES ? EX_NOPERM : EX_UNAVAILABLE);
	}

	tag = strrchr(progname, '/');
	if (!tag)
		tag = progname;
	else
		++tag;
	openlog(tag, LOG_CONS|(foreground ? LOG_PERROR : 0), SYSLOG_FACILITY);
	syslog(LOG_INFO, "mqrd %s started", VERSION);
	
	if (!foreground) {
		FILE *fp = NULL;

		if (pidfile) {
			fp = fopen(pidfile, "w");
			if (!fp) {
				fprintf(stderr,
					"%s: can't create pidfile %s: %s\n",
					progname, pidfile, strerror(errno));
				exit(errno == EACCES ?
				       EX_NOPERM : EX_CANTCREAT);
			}
		}

		if (daemon(1, 0)) {
			fprintf(stderr, "%s: can't become daemon: %s\n",
				progname, strerror(errno));
			exit(errno == EACCES ? EX_NOPERM : EX_UNAVAILABLE);
		}

		if (fp) {
			fprintf(fp, "%lu\n", (unsigned long) getpid());
			fclose(fp);
		}
	}

	set_signal(SIGTERM, sigterm);
	set_signal(SIGQUIT, sigterm);
	set_signal(SIGHUP, sigterm);
	
	if (!single_process)
		oldchild = set_signal(SIGCHLD, sigchild);
	
	while (!terminate) {
		struct sockaddr_un claddr;
		socklen_t clen = sizeof(claddr);
		int fd;
		FILE *inf;

		if (!single_process && nchildren >= maxchildren) {
			syslog(LOG_NOTICE, "too many children, pausing");
			pause();
			continue;
		}
		
		fd = accept(sd, (struct sockaddr*)&claddr, &clen);
		if (fd == -1) {
			if (errno == EINTR)
				continue;
			syslog(LOG_CRIT, "accept: %m");
			break;
		}

		inf = fdopen(fd, "w+");
		if (!inf) {
			syslog(LOG_ERR, "fdopen: %m");
			close(fd);
			continue;
		}

		if (!single_process) {
			pid_t pid = fork();

			if (pid == -1) {
				syslog(LOG_ERR, "fork: %m");
				fclose(inf);
				continue;
			}

			if (pid == 0) {
				set_signal(SIGCHLD, oldchild);
				process(inf);
				exit(0);
			}
			nchildren++;
		} else
			process(inf);

		fclose(inf);
	}
	syslog(LOG_INFO, "mqrd %s finished", VERSION);

	if (pidfile)
		unlink(pidfile);
	close(sd);
	unlink(sname);
	exit(0);
}
	
	
